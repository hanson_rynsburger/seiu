require "rails_helper"

describe Api::V1::UsersController, type: :controller do
  render_views

  before(:each) do
    allow(Api::V1::UserMailer).to receive(:password_reset).and_return({})
  end

  let(:user) { FactoryGirl.create(:user) }
  let(:api_key) { auth_with_user(user) }
  let(:validate_user) do
    request.env["HTTP_AUTHORIZATION"] = api_key.access_token
  end

  describe "GET #show" do
    context "with valid attributes" do
      it "renders show" do
        validate_user
        get :show, format: :json
        expect(response).to render_template(:show)
      end
    end

    context "with invalid access" do
      it "renders unauthorized" do
        get :show, format: :json, id: user.id
        expect(response).to render_template(:unauthorized)
      end
    end
  end

  describe "POST #create" do
    context "with valid attributes" do

      let(:user) { FactoryGirl.attributes_for(:user) }
      let(:valid_request) {
        post :create,
          format: :json,
          user: {
            first_name: 'Test',
            last_name: 'User',
            email: user[:email],
            password: user[:password],
            zip: user[:zip],
            county: user[:county],
            phone: user[:phone]
          }
      }

      it "creates a user" do
        expect { valid_request }
          .to change(User, :count).by(1)
      end

      it "creates an api_key" do
        expect { valid_request }
          .to change(ApiKey, :count).by(1)
      end
    end

    context "with invalid attributes" do
      let(:invalid_request) {
        post :create,
          format: :json,
          user: {
            email: "bademail",
            password: "123"
          }
      }

      it "does not creates a user" do
        expect { invalid_request }.to change(User, :count).by(0)
      end

      it "does not create an api_key" do
        expect { invalid_request }.to change(ApiKey, :count).by(0)
      end
    end
  end

  describe "POST #reset_password" do
    context "with valid attributes" do
      it "renders reset password" do
        post :reset_password, format: :json, user: { email: user.email }
        expect(response).to render_template(:reset_password)
      end
    end

    context "with invalid attributes" do
      it "renders error" do
        post :reset_password, format: :json, user: { email: nil }
        expect(response).to render_template(:error)
      end
    end
  end

  describe "POST #verify_password_token" do
    before(:each){ user.update(reset_password_token: "123") }
    context "with valid attributes" do
      it "renders valid response" do
        post :verify_password_token,
          format: :json,
          user: { reset_password_token: user.reset_password_token }
        expect(response).to render_template(:verify_password_token)
      end

      it "destroys verified password token" do
        post :verify_password_token,
          format: :json,
          user: { reset_password_token: user.reset_password_token }

        post :verify_password_token,
          format: :json,
          user: { reset_password_token: user.reset_password_token }

        expect(response).to render_template(:unauthorized)
      end
    end

    context "with invalid attributes" do
      it "renders error" do
        post :verify_password_token,
          format: :json,
          user: { reset_password_token: nil }
        expect(response).to render_template(:unauthorized)
      end
    end
  end
end
