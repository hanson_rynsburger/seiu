require "rails_helper"

describe Api::V1::EventsController, type: :controller do
  render_views

  let(:event) { FactoryGirl.create(:event) }

  describe "POST #create" do
    context "with valid attributes" do
      let(:valid_request) {
        post :create,
             format: :json,
             regionOrDept: 'IT',
             type: 'Training',
             eventName: 'testname',
             eventLocation: 'testplace',
             staffResponsible: 'tester',
             notes: 'testnotes',
             eventID: 10194,
             eventStartDate: '1/28/2016',
             eventStartTime: '12:15',
             eventStartAMPM: 'pm'
      }

      it "creates an event" do
        expect { valid_request }
          .to change(Event, :count).by(1)
      end

      it "renders create" do
        valid_request
        expect(response).to render_template(:create)
      end
    end

    context "with invalid attributes" do
      let(:invalid_request) {
        post :create,
             format: :json,
             event: {
               regionOrDept: 'IT',
               type: 'Training',
               eventName: 'testname',
               eventLocation: 'testplace',
               staffResponsible: 'tester',
               notes: 'testnotes',
             }
      }

      it "does not creates an event" do
        expect { invalid_request }.to change(Event, :count).by(0)
      end
    end
  end

  describe 'PATCH #update' do
    context 'with valid attributes' do
      let(:valid_request) {
        patch :update,
              format: :json,
              id: event.seiu_id,
              eventName: 'New Event'
      }

      it 'updates an event' do
        expect { valid_request }
          .to change{ Event.find(event.id).name }
                .from(event.name)
                .to('New Event')
      end

      it "renders update" do
        valid_request
        expect(response).to render_template(:update)
      end
    end
  end

end
