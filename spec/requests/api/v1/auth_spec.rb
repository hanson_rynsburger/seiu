require "rails_helper"

describe "Auth", type: :request do
  let(:user) { FactoryGirl.create(:user, password: "password123") }

  describe "POST /api/v1/auth" do
    describe "valid" do
      before(:each) do
        post "/api/v1/auth",
          format:   :json,
          user: {
            email:    user.email,
            password: "password123"
          }
      end

      it "returns 201", :show_in_doc do
        expect(response.status).to eq(201)
      end

      it "returns an access token" do
        expect(response.body).to include("access_token")
      end
    end

    describe "invalid email" do
      before(:each) do
        post "/api/v1/auth",
          format:   :json,
          user: {
            email:    "bademail",
            password: "password123"
          }
      end

      it "returns 401" do
        expect(response.status).to eq(401)
      end

      it "returns an error message" do
        expect(response.body).to include("Invalid Authentication Request")
      end
    end

    describe "invalid password" do
      before(:each) do
        post "/api/v1/auth",
          format:   :json,
          user: {
            email:    user.email,
            password: "badpassword"
          }
      end

      it "returns 401", :show_in_doc do
        expect(response.status).to eq(401)
      end

      it "returns an error message" do
        expect(response.body).to include("Invalid Authentication Request")
      end
    end
  end

  describe "DELETE /api/v1/auth" do
    let(:api_key) { auth_with_user(user) }

    describe "valid access_token" do
      before(:each) do
        delete "/api/v1/auth",
          { format: :json },
          "HTTP_AUTHORIZATION" => api_key.access_token
      end

      it "returns 200", :show_in_doc do
        expect_ok_status
      end
    end

    describe "invalid access_token" do
      before(:each) do
        delete "/api/v1/auth",
          { format: :json },
          "HTTP_AUTHORIZATION" => "1234"
      end

      it "returns a 200 OK", :show_in_doc do
        expect_ok_status
      end
    end

    describe "no access_token" do
      before(:each) do
        delete "/api/v1/auth",
          { format: :json }
      end

      it "returns a 200 OK" do
        expect_ok_status
      end
    end
  end
end
