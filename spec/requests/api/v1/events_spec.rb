require "rails_helper"

describe "Events", type: :request do

  describe "GET /api/v1/events" do
    let!(:user) { FactoryGirl.create(:user) }
    let(:api_key) { auth_with_user(user) }

    context "with valid user id" do
      before(:each) do
        get "/api/v1/events",
            { format: :json },
            "HTTP_AUTHORIZATION" => api_key.access_token
      end

      it "returns status 200", :show_in_doc do
        expect_ok_status
      end
    end

    context "with invalid user access token" do
      before(:each) do
        get "/api/v1/events",
            { format: :json }
      end

      it "returns status 204", :show_in_doc do
        expect_unauthorized_status
      end
    end
  end

  describe "POST /api/v1/events" do
    context "with valid attributes" do
      before(:each) do
        post "/api/v1/events",
             format:   :json,
             regionOrDept: 'IT',
             type: 'Training',
             eventName: 'testname',
             eventLocation: 'testplace',
             staffResponsible: 'tester',
             notes: 'testnotes',
             eventID: 10194,
             eventStartDate: '1/28/2016',
             eventStartTime: '12:15',
             eventStartAMPM: 'pm'
      end

      it "returns status 201", :show_in_doc do
        expect_created_status
      end

      it "returns a event object" do
        expect(json["event"])
          .to_not be_nil
      end
    end

    context "with invalid attributes" do
      context "when seiu_id is already taken" do
        before(:each) do
          FactoryGirl.create(:event, seiu_id: 123123)
          post "/api/v1/events",
               format: :json,
               regionOrDept: 'IT',
               type: 'Training',
               eventName: 'testname',
               eventLocation: 'testplace',
               staffResponsible: 'tester',
               notes: 'testnotes',
               eventID: 123123,
               eventStartDate: '1/28/2016',
               eventStartTime: '12:15',
               eventStartAMPM: 'pm'
        end

        it "returns status 422" do
          expect_error_status
        end

        it "returns error message", :show_in_doc do
          expect(response.body)
            .to include("has already been taken")
        end
      end
    end
  end

  describe "UPDATE /api/v1/events/:seiu_id" do
    context "with valid attributes" do
      let(:event) { FactoryGirl.create(:event) }

      before(:each) do
        patch "/api/v1/events/#{event.seiu_id}",
          format:   :json,
          eventName: 'Updated event'
      end

      it "returns status 200", :show_in_doc do
        expect_ok_status
      end

      it "returns a event object" do
        expect(json["event"])
          .to_not be_nil
      end
    end

    context "with invalid seiu_id" do
      before(:each) do
        patch "/api/v1/events/222222+",
               format: :json
      end

      it "returns status 204", :show_in_doc do
        expect_no_content_status
      end
    end
  end

  describe "DELETE /api/v1/events/:seiu_id" do
    let(:event) { FactoryGirl.create(:event) }

    context "with valid attributes" do
      before(:each) do
        delete "/api/v1/events/#{event.seiu_id}",
               format: :json
      end

      it "returns status 204", :show_in_doc do
        expect_no_content_status
      end
    end

    context "with invalid seiu_id" do
      before(:each) do
        delete "/api/v1/events/222222",
               format: :json
      end

      it "returns status 204" do
        expect_no_content_status
      end
    end
  end
end
