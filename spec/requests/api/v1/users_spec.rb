require "rails_helper"

describe "Users", type: :request do
  before(:each) do
    allow(Api::V1::UserMailer).to receive(:password_reset).and_return({})
  end

  describe "GET /api/v1/profile" do
    let!(:user) { FactoryGirl.create(:user) }
    let(:api_key) { auth_with_user(user) }

    context "with valid user id" do
      before(:each) do
        get "/api/v1/profile",
          { format: :json },
          "HTTP_AUTHORIZATION" => api_key.access_token
      end

      it "returns status 200", :show_in_doc do
        expect_ok_status
      end
    end

    context "with invalid user access token" do
      before(:each) do
        get "/api/v1/profile",
          { format: :json }
      end

      it "returns status 204", :show_in_doc do
        expect_unauthorized_status
      end
    end
  end

  describe "POST /api/v1/users" do
    context "with valid attributes" do
      before(:each) do
        post "/api/v1/users",
          format:   :json,
          user: {
            first_name: 'Test',
            last_name: 'User',
            email:    "test@test.com",
            password: "password123",
            zip: '58517',
            county: 'California',
            phone: '(186)285-7925'
          }
      end

      it "returns status 201", :show_in_doc do
        expect_created_status
      end

      it "returns a user object" do
        expect(json["user"])
          .to_not be_nil
      end

      it "returns an access token" do
        expect(json["access_token"])
          .to_not be_nil
      end
    end

    context "with invalid attributes" do
      context "when email is already taken" do
        before(:each) do
          FactoryGirl.create(:user, email: "test@test.com")
          post "/api/v1/users",
            format: :json,
            user: {
              email: "test@test.com",
              password: "password123",
              zip: '58517',
              county: 'California',
              phone: '(186)285-7925'
            }
        end

        it "returns status 422" do
          expect_unprocessable_status
        end

        it "returns error message" do
          expect(response.body)
            .to include("has already been taken")
        end
      end

      context "when email is an invalid format" do
        before(:each) do
          post "/api/v1/users",
            format: :json,
            user: {
              email: "bademailformat",
              password: "password123",
              zip: '58517',
              county: 'California',
              phone: '(186)285-7925'
            }
        end

        it "returns status 422", :show_in_doc do
          expect_unprocessable_status
        end

        it "returns error message" do
          expect(response.body)
            .to include("is invalid")
        end
      end

      context "when the password is too short" do
        before(:each) do
          post "/api/v1/users",
            format: :json,
            user: {
              email: "test@test.com",
              password: "123",
              zip: '58517',
              county: 'California',
              phone: '(186)285-7925'
            }
        end

        it "returns status 422" do
          expect_unprocessable_status
        end

        it "returns error message" do
          expect(response.body)
            .to include("is too short")
        end
      end
    end
  end

  describe "UPDATE /api/v1/users" do
    let(:api_key) { auth_with_user(user) }

    context "with valid attributes" do
      let(:user) { FactoryGirl.create(:user, email: "test1@test.com") }

      before(:each) do
        patch "/api/v1/profile",{
            format:   :json,
            user: {
              email:    "test@test.com",
              password: "password123",
              zip: '58517',
              county: 'California',
              phone: '(186)285-7925'
            }
          }, "HTTP_AUTHORIZATION" => api_key.access_token
      end

      it "returns status 200", :show_in_doc do
        expect_ok_status
      end

      it "returns a user object" do
        expect(json["user"])
          .to_not be_nil
      end
    end

    context "with invalid attributes" do
      let(:user) { FactoryGirl.create(:user, email: "test1@test.com") }

      context "when email is already taken" do
        let(:old_user) { FactoryGirl.create(:user, email: "test@test.com") }

        before(:each) do
          patch "/api/v1/profile",{
            format: :json,
            user: {
              email: old_user.email,
              password: "password123",
              zip: '58517',
              county: 'California',
              phone: '(186)285-7925'
            }
          }, "HTTP_AUTHORIZATION" => api_key.access_token
        end

        it "returns status 400", :show_in_doc do
          expect_unprocessable_status
        end

        it "returns error message" do
          expect(response.body)
            .to include("has already been taken")
        end
      end

      context "when email is an invalid format" do
        before(:each) do
          patch "/api/v1/profile",{
            format: :json,
            user: {
              email: "bademailformat",
              password: "password123",
              zip: '58517',
              county: 'California',
              phone: '(186)285-7925'
            }
          }, "HTTP_AUTHORIZATION" => api_key.access_token
        end

        it "returns status 400" do
          expect_unprocessable_status
        end

        it "returns error message" do
          expect(response.body)
            .to include("is invalid")
        end
      end
    end
  end

  describe "DELETE /api/v1/profile" do
    let(:user) { FactoryGirl.create(:user, email: "test2@test.com") }
    let(:api_key) { auth_with_user(user) }

    context "with valid attributes" do

      before(:each) do
        delete "/api/v1/profile",
          { format: :json },
          "HTTP_AUTHORIZATION" => api_key.access_token
      end

      it "returns status 204", :show_in_doc do
        expect_no_content_status
      end
    end

    context "with invalid access" do
      before(:each) do
        delete "/api/v1/profile",
          { format: :json }
      end

      it "returns status 401" do
        expect_unauthorized_status
      end
    end
  end

  describe "POST /api/v1/users/reset_password" do
    context "with valid attributes" do
      let(:user) { FactoryGirl.create(:user, email: "test1@test.com") }

      before(:each) do
        post "/api/v1/users/reset_password",{
            format:   :json,
            user: {
              email: user.email
            }
          }
      end

      it "returns status 200", :show_in_doc do
        expect_ok_status
      end
    end

    context "with invalid attributes" do
      before(:each) do
        post "/api/v1/users/reset_password",{
            format:   :json,
            user: {
              email: nil
            }
          }
      end

      it "returns status 400" do
        expect_error_status
      end
    end
  end

  describe "POST /api/v1/users/verify_password_token" do
    context "with valid attributes" do
      let(:user) { FactoryGirl.create(:user) }

      before(:each) do
        user.update(reset_password_token: "123")

        post "/api/v1/users/verify_password_token",{
            format:   :json,
            user: {
              reset_password_token: user.reset_password_token
            }
          }
      end

      it "returns status 200", :show_in_doc do
        expect_ok_status
      end
    end

    context "with invalid attributes" do
      before(:each) do
        post "/api/v1/users/verify_password_token",{
            format:   :json,
            user: {
              reset_password_token: nil
            }
          }
      end

      it "returns status 401" do
        expect_unauthorized_status
      end
    end
  end
end
