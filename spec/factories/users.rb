FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email    { Faker::Internet.email }
    password { Faker::Internet.password(8) }
    zip { Faker::Address.zip }
    county { Faker::Address.state }
    phone { Faker::PhoneNumber.cell_phone }
  end
end
