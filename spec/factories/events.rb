FactoryGirl.define do
  factory :event do
    seiu_id { Faker::Number.number(5) }
    region { Faker::Address.state }
    name { Faker::Lorem.sentence(3) }
    location { Faker::Address.street_address }
    start_time { Time.now }
    event_type { Faker::Lorem.words(1) }

    factory :recurring_event do
      recurring { true }
      end_time { Time.now + 1.weeks }
      days { [1] }
    end
  end
end
