require "rails_helper"

describe Event, type: :model do
  let(:event) { FactoryGirl.create(:event) }
  let(:recurring_event) { FactoryGirl.create(:recurring_event) }

  it "has a valid factory" do
    expect(event).to be_valid
  end

  it 'has a valid recurring factory' do
    expect(recurring_event).to be_valid
  end

  it "is invalid without a name" do
    event = FactoryGirl.build(:event, name: nil)
    expect(event).to_not be_valid
  end

  it "is invalid without a region" do
    event = FactoryGirl.build(:event, region: nil)
    expect(event).to_not be_valid
  end

  it "is invalid without a seiu_id" do
    event = FactoryGirl.build(:event, seiu_id: nil)
    expect(event).to_not be_valid
  end

  it "is invalid without a location" do
    event = FactoryGirl.build(:event, location: nil)
    expect(event).to_not be_valid
  end

  it "is invalid without a type" do
    event = FactoryGirl.build(:event, event_type: nil)
    expect(event).to_not be_valid
  end

  it "is invalid without start time" do
    event = FactoryGirl.build(:event, start_time: nil)
    expect(event).to_not be_valid
  end

  it "is invalid without a unique seiu id" do
    old_event = FactoryGirl.create :event, seiu_id: Faker::Number.number(5)
    new_event = FactoryGirl.build(:event,  seiu_id: old_event.seiu_id)
    expect(new_event).to_not be_valid
  end

  it 'is invalid without a recurring fields' do
    event = FactoryGirl.build(:event, recurring: true)
    expect(event).to_not be_valid
  end
end
