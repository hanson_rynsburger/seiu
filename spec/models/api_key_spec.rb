require "rails_helper"

describe ApiKey, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:api_key)).to be_valid
  end

  describe "#generate_access_token" do
    it "creates a unique access token" do
      old_key = FactoryGirl.create(:api_key, access_token: "12345")
      new_key = FactoryGirl.create(:api_key, access_token: "12345")

      expect(new_key.access_token).to_not eq("12345")
    end
  end

  describe ".clear_user_access_tokens" do
    let(:user) { FactoryGirl.create(:user) }

    it "destroys all of a user's access tokens" do
      FactoryGirl.create(:api_key, user: user)
      ApiKey::clear_user_access_tokens(user)

      expect(user.reload.api_key).to be_nil
    end
  end
end
