require "rails_helper"

describe User, type: :model do
  let(:user) { FactoryGirl.create(:user) }

  it "has a valid factory" do
    expect(user).to be_valid
    expect(user.language).to eq('en')
  end

  it "is invalid without an email address" do
    user = FactoryGirl.build(:user, email: nil)
    expect(user).to_not be_valid
  end

  it "is invalid without a unique email address" do
    old_user = FactoryGirl.create(:user, email: Faker::Internet.email)
    new_user = FactoryGirl.build(:user,  email: old_user.email)
    expect(new_user).to_not be_valid
  end

  it "is invalid without a password" do
    user = FactoryGirl.build(:user, password: nil)
    expect(user).to_not be_valid
  end

  it "is invalid with a short password" do
    user = FactoryGirl.build(:user, password: "123")
    expect(user).to_not be_valid
  end
end
