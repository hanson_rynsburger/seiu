module Translatable
  extend ActiveSupport::Concern

  included do
    after_save :update_translations
  end

  def update_translations
    fields = self.class::TRANSLATE_FIELDS || []
    if fields.map { |f| self.send("#{f.to_s}_changed?") }.all?
      ActiveRecord::Base.transaction do
        LANGUAGES.each do |l|
          translate = self.send("#{self.class.model_name.singular}_translations").find_or_initialize_by(language: l)
          values = fields.map { |f| self.send(f) }
          translated = EasyTranslate.translate values, to: l
          translated.each_with_index { |tt, ii| translate[fields[ii]] = tt }
          translate.save
        end
      end
    end unless Rails.env.test?
    true
  end
end