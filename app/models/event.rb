class Event < ActiveRecord::Base
  include Translatable

  has_many :event_translations, dependent: :destroy

  validates :name, :region, :event_type, :start_time, :location, presence: true
  validates :seiu_id, presence: true, uniqueness: true
  validates :days, :end_time, presence: true, if: :recurring?

  WEEKS = [:monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday]
  TRANSLATE_FIELDS = [:region, :name, :location, :event_type, :staff, :notes]

  scope :by_range, -> (from, to) { where('(start_time BETWEEN ? AND ?) OR (end_time BEWEEN ? AND ?)', from, to, from, to) }
  scope :singular, -> { where(recurring: false) }
  scope :recurring, -> { where(recurring: true) }

  default_scope { order(start_time: :desc) }

  def self.with_translation(lang = 'en')
    events = (lang == 'en') ? all : all.includes(:event_translations)
    events.map do |event|
      translation = unless lang == 'en'
                      event.event_translations.where(language: lang).first
                    end
      if event.recurring?
      else
      end
    end
    if lang == 'en'
      all.map(&:to_json)
    else
      all.includes(:event_translations).where(event_translation: {language: lang}).map do |e|
        json = e.to_json
        translation = e.event_translations.where(language: lang).first
        Event::TRANSLATE_FIELDS.each do |f|
          json[f] = translation[f]
        end unless translation.nil?
        json
      end
    end
  end

  # returns event json object with range, translation and including recurrings
  def self.get_events(from, to , lang = 'en')
    events = Event.by_range(from, to)
    events = events.includes(:event_translations) unless lang == 'en'
    singular_events = events.singular.as_json
    recurring_events = events.recurring.map do |event|
    end
  end

  def from_seiu(params)
    self.region = params['regionOrDept'] if params['regionOrDept']
    self.name = params['eventName'] if params['eventName']
    self.staff_only = params['staffOnly'] if params['staffOnly']
    self.event_type = params['type'] if params['type']
    self.location = params['eventLocation'] if params['eventLocation']
    self.recurring = params['recurring'] if params['recurring']
    self.staff = params['staffResponsible'] if params['staffResponsible']
    self.notes = params['notes'] if params['notes']
    self.seiu_id = params['eventID'] if params['eventID']
    self.start_time = parse_date_time(
      params['eventStartDate'], params['eventStartTime'], params['eventStartAMPM']
    ) if params['eventStartDate'] && params['eventStartTime'] && params['eventStartAMPM']
    if self.recurring
      self.end_time = parse_date_time(
        params['eventEndDate'], params['eventEndTime'], params['eventEndAMPM']
      ) if params['eventEndDate'] && params['eventEndTime'] && params['eventEndAMPM']
      self.days = WEEKS.collect { |w| WEEKS.index(w) if params[w] }.compact
    end

    self.save
  end

  def days2names
    self.days.map { |d| WEEKS[d] }
  end

  private

  def parse_date_time(date, time, pm)
    DateTime.strptime("#{date} #{time} #{pm.upcase}", '%m/%d/%Y %I:%M %p')
  end
end