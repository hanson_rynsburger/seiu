class EventTranslation < ActiveRecord::Base
  belongs_to :event

  validates :language, :region, :name, :staff, :notes, :event_type, :location, presence: true
end