class User < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable

  mount_uploader :avatar, AvatarUploader

  has_one :api_key, dependent: :destroy
  validates :email, :first_name, :last_name, :zip, :phone, :county, presence: true
  validates :email, uniqueness: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :password, length: { minimum: 8 }, on: :create

  def self.with_reset_password_token(params)
    unless params[:reset_password_token].blank?
      find_by(
        reset_password_token: params[:reset_password_token]
      )
    end
  end

  def self.find_existing_user(params)
    find_by(
      "email = ? or twitter_id = ? or facebook_id = ?",
      params[:email].to_s.downcase,
      params[:twitter_id],
      params[:facebook_id]
    )
  end

  # def self.from_facebook(params)
  #   api = Koala::Facebook::API.new params[:access_token]
  #   user = api.get_object('me')
  #   User.new({
  #     email: data.email,
  #     first_name: data.first_name,
  #     last_name: data.last_name,
  #     language: data.locale.split('_')[0]
  #   })
  # end

  def confirm!
    self.update(confirmed_at: Time.now)
  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
