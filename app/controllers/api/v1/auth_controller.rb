module Api::V1
  class AuthController < ApiController
    skip_before_filter :authenticate_user_from_token,
                       only: [:create, :destroy]

    skip_before_action :set_resource,
                       only: [:destroy]

    api :POST, '/auth', '[PUBLIC] Authorizes a user account.'
    param :user, Hash, required: true do
      param :email,    String, required: true
      param :password, String, required: true
      param :facebook_id, String, required: true
      param :twitter_id, String, required: true
    end
    description <<-EOS
      In case of authenticate with facebook/twitter account, you don't need to send email and password.
      Just send uid of facebook or twitter and you can get access token.
      If successful, it returns an <tt>access_token</tt> required by all
      subsequent requests, with status <tt>200</tt>.
      If unsuccessful, it returns status <tt>401</tt>.
    EOS
    def create
      if authenticate_user?
        find_or_create_api_key
      else
        error = { message: "Invalid Authentication Request" }
        render_unauthorized error
      end
    end

    api :DELETE, '/auth', 'Deauthorizes a user account.'
    description 'Destroys Access Token for user.'
    def destroy
      access_token = request.headers["HTTP_AUTHORIZATION"]
      ApiKey.where(access_token: access_token).destroy_all
    end

    private

    def user
      @user ||= User.find_existing_user(auth_params)
    end

    def with_social?
      auth_params[:facebook_id].present? || auth_params[:twitter_id].present?
    end

    def authenticate_user?
      user && (with_social? || user.valid_password?(auth_params[:password]))
    end

    def find_or_create_api_key
      @api_key = ApiKey.where(user: user).first_or_create
      render status: 201
    end

    def resource_class
      "api_key".classify.constantize
    end

    def set_resource(resource = nil)
      resource ||= resource_class.find_by(id: auth_params[:id].to_i)
      instance_variable_set("@#{resource_name}", resource)
    end

    def auth_params
      params.require(:user).permit(:email, :password, :twitter_id, :facebook_id)
    end
  end
end
