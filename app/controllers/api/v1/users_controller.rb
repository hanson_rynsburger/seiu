module Api::V1
  class UsersController < ApiController
    require 'securerandom'
    include ApipieDescriptions

    skip_before_filter :authenticate_user_from_token,
      only: [:create, :reset_password, :verify_password_token, :verify_email_token]

    apipie_users_index
    apipie_users_show
    apipie_users_create
    apipie_users_update
    apipie_users_destroy
    apipie_users_reset_password
    apipie_users_verify_password_token
    apipie_users_resend_verification_email

    def create
      set_resource(resource_class.new(resource_params))
      if get_resource.save
        create_api_key
        send_verification_email(get_resource)
        render :create, status: :created
      else
        render_unprocessable
      end
    end

    # def facebook
    #   set_resource(resource_class.from_facebook(resource_params))
    #   if get_resource.save(validate: false)
    #     create_api_key
    #     send_verification_email(get_resource)
    #     render :create, status: :created
    #   else
    #     render_unprocessable
    #   end
    # end

    def update
      if get_resource.update(user_params)
      else
        render_unprocessable
      end
    end

    def reset_password
      if user = User.find_by(email: user_params[:email])
        send_password_reset_email(user)
      else
        render_error "Email address not found."
      end
    end

    def verify_password_token
      if @user = User.with_reset_password_token(user_params)
        @user.update(
          reset_password_token: nil,
          reset_password_sent_at: nil
        )
        create_api_key
      else
        render_unauthorized
      end
    end

    def verify_email_token
      user = User.find_by(
        confirmation_token: params[:token]
      )
      user.confirm!

      send_welcome_email(user)

      redirect_to thank_you_path
    end

    def resend_verification_email
      if user = User.find(params[:id])
        UserMailer.verify_email(user)
      else
        render_unauthorized
      end
    end

    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :reset_password_token, :avatar,
        :zip, :county, :phone, :password, :confirmation_token, :language, :facebook_id, :twitter_id)
    end

    private

    def create_api_key
      @api_key = ApiKey.create(user: @user)
    end

    def send_verification_email(user)
      user.update({
        confirmation_token: generate_email_verification_token(user)
      })

      UserMailer.verify_email(user)
    end

    def send_welcome_email(user)
      UserMailer.welcome_email(user)
    end

    def send_password_reset_email(user)
      user.update({
        reset_password_token: generate_reset_password_token(user),
        reset_password_sent_at: Time.now
      })

      UserMailer.password_reset(user)
    end

    def generate_reset_password_token(user)
      begin
        reset_password_token = secure_token
      end while User.find_by(reset_password_token: reset_password_token)

      reset_password_token
    end

    def generate_email_verification_token(user)
      begin
        confirmation_token = secure_token
      end while User.find_by(confirmation_token: confirmation_token)

      confirmation_token
    end

    def secure_token
      SecureRandom.hex(12)
    end

    def set_resource(resource = nil)
      @user = (resource || current_user)
    end
  end
end
