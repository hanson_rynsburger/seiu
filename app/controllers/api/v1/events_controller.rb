module Api::V1
  class EventsController < ApiController
    include ApipieDescriptions
    before_filter :set_event, only: [:update, :destroy]
    skip_before_filter :authenticate_user_from_token, except: [:index]
    skip_before_filter :set_resource, only: [:update, :destroy]

    apipie_events_index
    apipie_events_create
    apipie_events_update
    apipie_events_destroy
    apipie_events_destroy

    def index
      @from = Date.parse(params[:from] || Date.today.to_s).beginning_of_day
      @to = Date.parse(params[:to] || Date.today.to_s).end_of_day
      events = Event.by_range(from, to).with_translation
      regular_events = events.select { |e| !e[:recurring] }
      recurring_events = events.select { |e| e[:recurring] }
      recurring_events.each do |e|
        schedule = IceCube::Schedule.new()
      end
    end

    def create
      set_resource Event.new
      if get_resource.from_seiu(params)
        render :create, status: :created
      else
        render_error get_resource.errors
      end
    end

    def update
      if get_resource.from_seiu(params)
        render :update
      else
        render_error get_resource.errors
      end
    end

    private

    def event_params
      params.require(:event).permit(:seiu_id, :region, :name, :location, :date, :time, :type, :staff, :notes)
    end

    def set_event
      set_resource Event.find_by(seiu_id: params[:id])
    end
  end
end