json.success true
json.status 201
json.event do
  json.seiu_id @event.seiu_id
  json.region @event.region
  json.name @event.name
  json.location @event.location
  json.start_time @event.start_time
  json.type @event.event_type
  json.staff @event.staff
  json.staff_only @event.staff_only
  json.notes @event.notes
  json.recurring @event.recurring
  if @event.recurring?
    json.end_time @event.end_time
    json.days @event.days2names
  end
end
