json.events @events do |event|
  json.region event.region
  json.name event.name
  json.location event.location
  json.start_time event.start_time
  json.type event.event_type
  json.staff event.staff
  json.staff_only event.staff_only
  json.notes event.notes
  json.created_at event.created_at
  json.updated_at event.updated_at
  json.recurring event.recurring
  if event.recurring?
    json.end_time event.end_time
    json.days event.days2names
  end

  json.translations event.event_translations do |t|
    json.name t.name
    json.location t.location
    json.region t.region
    json.type t.event_type
    json.staff t.staff
    json.notes t.notes
  end
end
