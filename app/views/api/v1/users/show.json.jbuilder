json.success true
json.status 201
json.user do
  json.id @user.id
  json.email @user.email
  json.first_name @user.first_name
  json.last_name @user.last_name
  json.verified @user.confirmed_at.present?
  json.zip @user.zip
  json.county @user.county
  json.phone @user.phone
  json.avatar @user.avatar.url
end
