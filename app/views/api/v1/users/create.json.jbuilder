json.success true
json.status 201
json.access_token @api_key.access_token
json.user do
  json.id @user.id
  json.email @user.email
  json.first_name @user.first_name
  json.last_name @user.last_name
  json.zip @user.zip
  json.county @user.county
  json.phone @user.phone
  json.avatar @user.avatar.url
  json.verified @user.confirmed_at.present?
end
