module Api::V1
  class UserMailer
    require "mandrill"
    extend ActiveSupport::NumberHelper

    def self.mandrill_client
      @mandrill_client ||= Mandrill::API.new(ENV['MANDRILL_APIKEY'])
    end

    def self.password_reset(user)
      template_name = "reset-password"
      url = Rails.application.routes.url_helpers.verify_password_token_v1_users_url(user: {reset_password_token: user.reset_password_token})

      message = {
        to: [{
               email: user.email,
               name: user.full_name
             }],
        from_email: "noreply@seiu2015.org",
        from_name: "SEIU Password Recovery",
        subject: "You've requested to reset your password",
        merge_vars: [
          { rcpt: user.email,
            vars: [
              { name: "FULL_NAME",  content: user.full_name },
              { name: "URL",        content: url }
            ]
          }
        ]
      }

      mandrill_client.messages.send_template(template_name, "", message)
    end

    def self.welcome_email(user)
      template_name = "welcome"

      message = {
        to: [{
               email: user.email,
               name: user.full_name
             }],
        from_email: "noreply@seiu2015.org",
        from_name: "SEIU App",
        subject: "Welcome to SEIU!",
        merge_vars: [
          { rcpt: user.email,
            vars: [
              { name: "FULL_NAME",       content: user.full_name },
              { name: "EMAIL",           content: user.email },
              { phone: "PHONE",          content: user.phone }
            ]
          }
        ]
      }

      mandrill_client.messages.send_template(template_name, "", message)
    end

    def self.verify_email(user)
      template_name = "verify-email"
      url = Rails.application.routes.url_helpers.verify_email_v1_users_url(user: {confirmation_token: user.confirmation_token})

      message = {
        to: [{
               email: user.email,
               name: user.full_name
             }],
        from_email: "noreply@seiu2015.org",
        from_name: "SEIU App",
        subject: "Verify your email address",
        merge_vars: [
          { rcpt: user.email,
            vars: [
              { name: "URL", content: url }
            ]
          }
        ]
      }

      mandrill_client.messages.send_template(template_name, "", message)
    end
  end
end
