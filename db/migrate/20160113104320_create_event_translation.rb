class CreateEventTranslation < ActiveRecord::Migration
  def change
    create_table :event_translations do |t|
      t.references :event
      t.string :language
      t.string :name
      t.string :region
      t.string :location
      t.string :event_type
      t.string :staff
      t.text :notes

      t.timestamps
    end
  end
end
