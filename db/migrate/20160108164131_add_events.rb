class AddEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :seiu_id
      t.string :region
      t.string :name
      t.string :location
      t.date :date
      t.time :time
      t.string :type
      t.string :staff
      t.string :notes

      t.timestamps
    end

    add_index :events, :seiu_id
  end
end
