class AddFacebookTwitterToUser < ActiveRecord::Migration
  def change
    add_column :users, :twitter_id, :string
    add_column :users, :facebook_id, :string
    add_column :users, :language, :string, default: 'en'
  end
end
