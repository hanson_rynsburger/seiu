class AddRecurringFieldsToEvent < ActiveRecord::Migration
  def change
    add_column :events, :days, :integer, array: true, default: []
    remove_column :events, :date, :date
    remove_column :events, :time, :time
    add_column :events, :start_time, :datetime
    add_column :events, :end_time, :datetime
    add_column :events, :recurring, :boolean, default: false
    add_column :events, :staff_only, :boolean, default: false
    remove_column :events, :notes, :string
    add_column :events, :notes, :text
    rename_column :events, :type, :event_type
  end
end
