class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :zip, :string
    add_column :users, :phone, :string
    add_column :users, :avatar, :string
    add_column :users, :county, :string
  end
end
