Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  apipie

  get '/thank-you' => 'pages#thank_you', as: :thank_you

  scope module: 'api', defaults: { format: :json } do
    scope 'api' do
      namespace :v1 do
        resource :auth, only: [:create, :destroy], controller: :auth

        resource :users, only: [] do
          post 'reset_password' => 'users#reset_password'
          post 'verify_password_token' => 'users#verify_password_token'
          get 'verify_email' => 'users#verify_email'
          post 'resend_verification_email' => 'users#resend_verification_email'
        end

        get 'profile' => 'users#show'
        patch 'profile' => 'users#update'
        delete 'profile' => 'users#destroy'

        resources :users, only: [:create]
        resources :events
      end
    end
  end
end
