module ApipieDescriptions::Users
  def apipie_users_index
    apipie_clear(__method__)
  end

  def apipie_users_show
    api :GET, "/profile", "Retrieves a User object."
    description <<-EOS
      If successful, it returns a <tt>user</tt> object based with
      status <tt>200</tt>.
    EOS

    apipie_clear(__method__)
  end

  def apipie_users_create
    api :POST, "/users", "[PUBLIC] Registers a new User."
    param :user, Hash, required: true do
      param :email, String, required: true
      param :password, String, required: true
      param :first_name, String, required: true
      param :last_name, String, required: true
      param :zip, String, required: true
      param :county, String, required: true
      param :phone, String, required: true
      param :language, String, required: true
      param :avatar, File
      param :facebook_id, String
      param :twitter_id, String
    end
    description <<-EOS
      If successful, it returns a <tt></tt> object with status <tt>201</tt>.
      If unsuccessful, it returns an errors hash with status <tt>400</tt>.
    EOS

    apipie_clear(__method__)
  end

  def apipie_users_update
    api :PATCH, "/profile", "Updates profile."
    param :user, Hash, required: true do
      param :email, String
      param :password, String
      param :first_name, String
      param :last_name, String
      param :zip, String
      param :county, String
      param :phone, String
      param :avatar, File
      param :facebook_id, String
      param :twitter_id, String
    end
    description <<-EOS
      You can disconnect facebook or twitter by sending nil value to facebook_id and twitter_id field.
      If successful, it returns a <tt></tt> object with status <tt>201</tt>.
      If unsuccessful, it returns an errors hash with status <tt>400</tt>.
    EOS

    apipie_clear(__method__)
  end

  def apipie_users_destroy
    api :DELETE, "/profile", "Deletes a User."
    description <<-EOS
      Always returns status <tt>204</tt>.
    EOS
    apipie_clear(__method__)
  end

  def apipie_users_reset_password
    api :POST, "/users/reset_password", "Reset a user's password."
    param :user, Hash, required: true do
      param :email, String, required: true
    end
    description <<-EOS
      If successful, it returns status <tt>200</tt> and sends an email to the
      user with a reset password token.
      If unsuccessful, it returns an errors hash with status <tt>400</tt>.
    EOS
    apipie_clear(__method__)
  end

  def apipie_users_verify_password_token
    api :POST, "/users/verify_password_token", "Verify a password reset token."
    param :user, Hash, required: true do
      param :reset_password_token, String, required: true
    end
    description <<-EOS
      If successful, it returns the User object and a new API authentication
      key.
      If unsuccessful, it returns Unauthorized <tt>401</tt>.
    EOS
    apipie_clear(__method__)
  end

  def apipie_users_resend_verification_email
    api :POST, "/users/:id/resend_verification_email", "Resend email verification email"
    param :id, Integer
    description <<-EOS
      If successful, it returns status 200.
      If unsuccessful, it returns No Content <tt>401</tt>.
    EOS
    apipie_clear(__method__)
  end
end
