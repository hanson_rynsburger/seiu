module ApipieDescriptions::Events
  def apipie_events_index
    api :GET, '/events', 'Retrieve events.'
    param :from, Date
    param :to, Date
    description <<-EOS
      It returns <tt>event list</tt>, with status <tt>200</tt>.
    EOS

    apipie_clear(__method__)
  end

  def apipie_events_create
    api :POST, '/events', '[SEIU] Creates a new event object, with status 201'
    description <<-EOS
      For recurring work orders, please check fields with recurring mark.
      If successful, it returns a <tt>event</tt> object, with status
      <tt>201</tt>

      If unsuccessful, it returns with status <tt>422</tt>
    EOS
    param :regionOrDept, String, required: true
    param :staffOnly, [true, false]
    param :type, String, required: true
    param :eventName, String, required: true
    param :eventLocation, String, required: true
    param :recurring, [true, false], desc: 'Default: <tt>false</tt>'
    param :staffResponsible, String
    param :notes, String
    param :eventId, Integer, required: true
    param :eventStartDate, String, required: true, desc: 'Format: <tt>M/D/YYYY</tt>'
    param :eventStartTime, String, required: true, desc: 'Format: <tt>HH:MM</tt>'
    param :eventStartAMPM, ['am', 'pm'], required: true
    param :eventEndDate, String, required: true, desc: '[RECURRING] Format: <tt>M/D/YYYY</tt>'
    param :eventEndTime, String, required: true, desc: '[RECURRING] Format: <tt>HH:MM</tt>'
    param :eventEndAMPM, String, required: true, desc: '[RECURRING] Format: <tt>am</tt> or <tt>pm</tt>'
    Event::WEEKS.each { |w| param w, [true, false], desc: '[RECURRING]' }

    apipie_clear(__method__)
  end

  def apipie_events_update
    api :PATCH, '/events/:seiu_id', '[SEIU] updates an existing event with seiu_id, with status 200'
    description <<-EOS
      If successful, it returns a <tt>event</tt> object, with status <tt>200</tt>

      If unsuccessful, it returns with status <tt>422</tt>
    EOS
    param :seiu_id, Integer, required: true

    param :regionOrDept, String, required: true
    param :staffOnly, [true, false]
    param :type, String, required: true
    param :eventName, String, required: true
    param :eventLocation, String, required: true
    param :recurring, [true, false], desc: 'Default: <tt>false</tt>'
    param :staffResponsible, String
    param :notes, String
    param :eventId, Integer, required: true
    param :eventStartDate, String, required: true, desc: 'Format: <tt>M/D/YYYY</tt>'
    param :eventStartTime, String, required: true, desc: 'Format: <tt>HH:MM</tt>'
    param :eventStartAMPM, ['am', 'pm'], required: true
    param :eventEndDate, String, required: true, desc: '[RECURRING] Format: <tt>M/D/YYYY</tt>'
    param :eventEndTime, String, required: true, desc: '[RECURRING] Format: <tt>HH:MM</tt>'
    param :eventEndAMPM, String, required: true, desc: '[RECURRING] Format: <tt>am</tt> or <tt>pm</tt>'
    Event::WEEKS.each { |w| param w, [true, false], desc: '[RECURRING]' }

    apipie_clear(__method__)
  end

  def apipie_events_destroy
    api :DESTROY, '/events/:seiu_id', '[SEIU] destroys an existing event, with status 204'
    description <<-EOS
      If successful, it returns no content, with status <tt>204</tt>

      If unsuccesful, it returns no content, with status <tt>204</tt>
    EOS
    param :seiu_id, Integer, required: true

    apipie_clear(__method__)
  end
end